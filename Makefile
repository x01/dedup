SRC=main.cpp sha2/sha2.c
CXXOPT=-std=c++14 -I. -g
COPT=-std=c99 -I. -g

_build/depdup : _build/main.o _build/sha2.o
	clang++ -g -lboost_system -lboost_filesystem $(CXXOPT) $^ -o _build/depdup 

_build/main.o : main.cpp _build
	clang++ -c $(CXXOPT) $< -o $@

_build:
	mkdir _build

_build/sha2.o : sha2/sha2.c sha2/sha2.h
	clang -c $(COPT) $< -o $@

