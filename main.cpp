#include <boost/filesystem.hpp>
#include <sha2/sha2.h>
#include <set>
#include <vector>
#include <memory>

namespace fs = boost::filesystem;
namespace sys = boost::system;

int g_level = 0;
void log(int level, char const* fmt, ...) {
	if (g_level >= level) {
		va_list vl;
		va_start(vl, fmt);
		vprintf(fmt, vl);
		va_end(vl);
	}
}

std::string s(fs::path const& p) {
	return p.string<std::string>();
}

struct Sha2Digest {
   	uint8_t data[SHA256_DIGEST_LENGTH];
	static int hh(int v) { return v > 9 ? 'a' + v - 0xa : '0' + v; }
	void to_string(char buffer[SHA256_DIGEST_LENGTH*2+1]) const {
		for (int i=0; i<SHA256_DIGEST_LENGTH; ++i) {
			buffer[i*2] = hh((data[i]&0xf0) >> 4);
			buffer[i*2 + 1] = hh(data[i]&0xf);
		}
		buffer[SHA256_DIGEST_LENGTH*2] = 0;

	}
	std::string to_string() const {
		char buffer[SHA256_DIGEST_LENGTH*2+1];
		to_string(buffer);
		return std::string(buffer, SHA256_DIGEST_LENGTH*2);
	}
   	bool operator<(Sha2Digest const& a) const {
		return memcmp(data, a.data, sizeof(a.data)) < 0;
	}
   	bool operator==(Sha2Digest const& a) const {
		return memcmp(data, a.data, sizeof(a.data)) == 0;
	}
};

std::ostream& operator<<(std::ostream& os, Sha2Digest const& d) {
	char buffer[SHA256_DIGEST_LENGTH*2+1];
	d.to_string(buffer);
	os << buffer;
	return os;
}

struct Sha2Context {

	SHA256_CTX m_ctx;

	Sha2Context () { SHA256_Init(&m_ctx); }

//	template<class T>
//	std::enable_if<std::is_fundamental<T>::value, Sha2Context>::type&
// 	operator<<(T const& a) { SHA256_Update(&m_ctx, &a, sizeof(a));

	void feed(void const* p, size_t s) { SHA256_Update(&m_ctx, reinterpret_cast<uint8_t const*>(p), s); }
	Sha2Digest finish() { Sha2Digest d; SHA256_Final(d.data, &m_ctx); return d; }

	void feed_file(fs::path const& p, sys::error_code& ec) {
		fs::fstream in (p);

		Sha2Digest digest;
		if (!in.good()) {
			ec = sys::errc::make_error_code(sys::errc::no_such_file_or_directory);
			return;
		}

		ec.clear();
		size_t size = fs::file_size(p);
		if (size == 0)
			return;

		for (size_t left = size; left > 0;) {
			constexpr size_t SZ = 1024*1024*4;
			std::unique_ptr<char[]> buffer (new char[SZ]);
			size_t toread = std::min(left, SZ);
			in.read (buffer.get(), toread);
			if (!in.good()) {
				ec = sys::errc::make_error_code(sys::errc::io_error);
				return;
			}

			feed (buffer.get(), toread);

			left -= toread;
		}
	}
};

struct FileList
{
	std::vector<std::string> m_exts = { ".jpg", ".jpeg", ".pef" };

	struct Info
	{
		fs::path path;
		Sha2Digest hash;

		bool operator<(Info const& a) const { return hash < a.hash; }
		bool operator==(Info const& a) const { return hash == a.hash; }
	};

	struct SortByPathName
	{
		bool operator()(Info const& a, Info const& b) const { return a.path < b.path; }
	};

	struct CmpDeref
	{
		bool operator()(Info const* a, Info const* b) const { return a->hash < b->hash; }
	};

	void add_file(fs::path const& p) {
		Sha2Context sha;
		sys::error_code ec;
		sha.feed_file(p, ec);
		if (ec)
			return;

		Sha2Digest d = sha.finish();

		log(2, "%s: %s\n", s(p).c_str(), d.to_string().c_str());
		Info i { p, d };
		files.insert (i);
	}
	
	static std::string tolower(fs::path const& p) {
		std::string res;
		res.resize (p.size());
		for (int i=0; i<p.size(); ++i)
			res[i] = ::tolower(s(p).c_str()[i]);
		return res;
	}

	void scan(fs::path const& p) {
		for (fs::directory_iterator it(p), end; it != end; ++it) {

			fs::path const& p = it->path();
			if (it->status().type() == fs::directory_file)
				scan (p);
			else if (it->status().type() == fs::regular_file) {
				for(auto&& e : m_exts) {
					std::string ext = tolower(p.extension());
					if (strcmp(ext.c_str(), e.c_str()) == 0) {
						add_file (p);
						break;
					}
				}
			}
		}
	}

//	enum CmpOptions
//	{
//		IncludeLeftUnique = 0x01,
//		IncludeRightUnique = 0x02,
//		IncludeEqual = 0x04
//	};

	typedef std::set<Info> Set;

	static Set duplicates(FileList& a, FileList& b)
	{
		Set output;
		std::set_intersection (b.files.begin(), b.files.end(), a.files.begin(), a.files.end(),
				std::inserter(output, output.end()));
		return output;
	}

	static Set right_unique(FileList& a, FileList& b)
	{
		Set output;
		std::set_difference (b.files.begin(), b.files.end(), a.files.begin(), a.files.end(),
				std::inserter(output, output.end()));
		return output;
	}

	static Set left_unique(FileList& a, FileList& b)
	{
		Set output;
		std::set_difference (a.files.begin(), a.files.end(), b.files.begin(), b.files.end(),
				std::inserter(output, output.end()));
		return output;
	}

	static Set unique(FileList& a, FileList& b)
	{
		Set output;
		std::set_difference (b.files.begin(), b.files.end(), a.files.begin(), a.files.end(),
				std::inserter(output, output.end()));
		std::set_difference (a.files.begin(), a.files.end(), b.files.begin(), b.files.end(),
				std::inserter(output, output.end()));
		return output;
	}

	static Set merge(FileList& a, FileList& b)
	{
		Set output;
		std::merge (b.files.begin(), b.files.end(), a.files.begin(), a.files.end(),
				std::inserter(output, output.end()));
		return output;
	}

	Set files;
};

struct OperationImpl
{
	virtual void initialize(sys::error_code& ec) = 0;
	virtual void perform(FileList::Info const& i, sys::error_code& ec) = 0;
};

struct CopyOperation : OperationImpl
{
	fs::path destFolder;
	CopyOperation(fs::path const& p) : destFolder(p) {}

	virtual void initialize(sys::error_code& ec) {
		ec.clear();
		if (!fs::exists(destFolder)) {
			fs::create_directory(destFolder, ec);
			if (ec) {
				log(0, "Failed to create %s. %s\n", s(destFolder).c_str(), ec.message().c_str());
				return;
			}
		}
	}
	virtual void perform(FileList::Info const& i, sys::error_code& ec) {
		ec.clear();
		fs::path dest = destFolder / i.path.filename();
		log (0, "copy %s to %s\n", s(i.path).c_str(), s(dest).c_str());
		fs::copy(i.path, dest, ec);
		if (ec) {
			log(0, "Failed to copy %s to %s. %s\n", s(i.path).c_str(), s(dest).c_str(), ec.message().c_str());
			return;
		}
		return;
	}
};

struct ListOperation : OperationImpl
{
	virtual void initialize(sys::error_code& ec) {
		ec.clear();
	}
	virtual void perform(FileList::Info const& i, sys::error_code& ec) {
		log (0, "%s\n", s(i.path).c_str());
		ec.clear();
	}
};

struct Options
{
	enum class Iterate { None, LeftUnique, RightUnique, Unique, Duplicate, Merge };
	enum class Operation { None, List, Copy };

	Iterate iterate = Iterate::None;

	bool parse_iterate(char const* a) {
		if (strcmp(a, "none") == 0) iterate = Iterate::None;
		else if (strcmp(a, "left-unique") == 0) iterate = Iterate::LeftUnique;
		else if (strcmp(a, "right-unique") == 0) iterate = Iterate::RightUnique;
		else if (strcmp(a, "unique") == 0) iterate = Iterate::Unique;
		else if (strcmp(a, "duplicates") == 0) iterate = Iterate::Duplicate;
		else if (strcmp(a, "merge") == 0) iterate = Iterate::Merge;
		else return false;
		return true;
	}

	bool parse_operation(int& i, char ** parg, int narg) {
		if (strcmp(parg[i], "copy") == 0) {
			if (++i == narg) {
				log(0, "copy operation: target folder missing\n");
				return false;
			}
			auto uni = std::make_unique<CopyOperation>(parg[i]);
			operations.push_back(std::move(uni));
		} else if (strcmp(parg[i], "list") == 0) {
			auto uni = std::make_unique<ListOperation>();
			operations.push_back(std::move(uni));
		} else return false;
		return true;
	}

	void init_ops(sys::error_code& ec) {
		for (auto&& o : operations) {
			o->initialize(ec);
			if (ec) return;
		}
	}
	void perform(FileList::Info const& i, sys::error_code& ec) {
		for (auto&& o : operations) {
			o->perform(i, ec);
			if (ec) return;
		}
	}

	std::vector<std::unique_ptr<OperationImpl>> operations;
};

Options opts;

int main(int narg, char** parg)
{
	std::string uniq;

	if (narg < 3) {
		printf("Usage: <folder1> <folder2> [-i none|left-unique|right-unique|unique|duplicates|merge]\n"
			   "[-op <copy dest> | <list>]\n");
		return 1;
	}

	for (int i=1; i<narg; ++i) {
		if (strcmp(parg[i], "-i") == 0) {
			if (++i == narg) {
				log(0, "iteration needs mode\n");
				return 1;
			}
			if (!opts.parse_iterate(parg[i])) {
				log(0, "error parsing iterate mode\n");
				return 1;
			}
		} else if (strcmp(parg[i], "-op") == 0) { 
			if (++i == narg) {
				log(0, "specify operation\n");
				return 1;
			}
			if (!opts.parse_operation(i, parg, narg)) {
				log(0, "error parsing operation\n");
				return 1;
			}
		} else if (strcmp(parg[i], "-v") == 0) {
			g_level++;
		}
	}
	
	std::vector<fs::path> folders;
	for (int i=1; i<std::min(3, narg); ++i)
		folders.push_back(parg[i]);

	FileList fl[2];
	for (int i=0; i<folders.size(); ++i)
	{
		log(1, "scanning %s\n", s(folders[i]).c_str());
		fl[i].scan (folders[i]);

		log (1, "found %d files in %s\n", (int)fl[i].files.size(), s(folders[i]).c_str());
	}

	FileList::Set opList;

	switch (opts.iterate)
	{
		case Options::Iterate::None:
		break;

		case Options::Iterate::LeftUnique:
			opList = FileList::left_unique(fl[0], fl[1]);
		break;

		case Options::Iterate::RightUnique:
			opList = FileList::right_unique(fl[0], fl[1]);
		break;

		case Options::Iterate::Duplicate:
			opList = FileList::duplicates(fl[0], fl[1]);
		break;

		case Options::Iterate::Unique:
			opList = FileList::unique(fl[0], fl[1]);
		break;
		
		case Options::Iterate::Merge:
			opList = FileList::merge(fl[0], fl[1]);
		break;
	}

	log(0, "operating on %d files\n", (int)opList.size());
	std::vector<FileList::Info> sorted(opList.begin(), opList.end());
	std::stable_sort(sorted.begin(), sorted.end(), FileList::SortByPathName());

	sys::error_code ec;
	opts.init_ops(ec);
	if (ec) {
		log(0, "failed to performa operation. %s\n", ec.message().c_str());
		return 1;
	}
	for (FileList::Info const& i : sorted)
	{
		opts.perform(i, ec);
		if (ec) {
			log(0, "failed to performa operation. %s\n", ec.message().c_str());
			return 1;
		}
	}

	return 0;
}
